const Task = require("../model/task");

module.exports = class TaskController {
    addTask(req, res) {
        const task = new Task({
            text: req.body.text
        });

        task.save(function (err, doc) {
            if (err) {
                res.send(err)
            }

            res.send(doc)
        })
    }

    getTasks(req, res) {
        Task.find({}, (err, doc) => {
            if (err) {
                res.send(err)
            }

            res.send(doc)
        })
    }

    updateTask(req, res) {
        const id = req.params.id;
        Task.findByIdAndUpdate(id, {status: true}, function (err, doc) {
            if (err) {
                res.send(err)
            }

            res.send(doc)
        })
    }

    deleteTask(req, res) {
        const id = req.params.id;
        Task.findByIdAndRemove(id, function (err, doc) {
            if (err) {
                res.send(err)
            }

            res.send(doc)
        })
    }
}