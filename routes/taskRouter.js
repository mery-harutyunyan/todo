const express = require("express");
const taskController = require("../controllers/taskController");
const taskRouter = express.Router();
let TaskController = new taskController();

taskRouter.post('/create', (req, res) => TaskController.addTask(req, res));
taskRouter.get('/tasks', (req, res) =>TaskController.getTasks(req, res));
taskRouter.put('/tasks/:id', (req, res) => TaskController.updateTask(req, res));
taskRouter.delete('/tasks/:id', (req, res) => TaskController.deleteTask(req, res));

module.exports = taskRouter;